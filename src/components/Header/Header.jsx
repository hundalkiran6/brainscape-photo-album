import React from "react";
import "./Header.css";

const Header = () => {
  return (
    <header className="App-header">
      <h1>Brainscape App Coding Exercise</h1>
      <p>Photo Album Generator by Kiran H.</p>
    </header>
  );
};

export default Header;
