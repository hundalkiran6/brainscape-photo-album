import React from "react";

export const ImageGallery = React.lazy(() =>
  import("./ImageGallery/ImageGallery")
);

export const AlbumGenerator = React.lazy(() =>
  import("./Album/AlbumGenerator")
);
