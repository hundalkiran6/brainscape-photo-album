import React from "react";
import { useDrop } from "react-dnd";
import AlbumLabels from "./AlbumLabels";
import AlbumImages from "./AlbumImages";

import "./Album.css";

const AlbumGenerator = ({ selectedImages, onImageSelect }) => {
  const [, drop] = useDrop({
    accept: "IMAGE",
    drop: (item, monitor) => {
      if (monitor.canDrop()) {
        onImageSelect(item.image, false); // false indicates addition
      }
    },
  });

  return (
    <aside className="album" ref={drop}>
      <h1 className="album-header">Album Generator</h1>
      <div className="album-container">
        <AlbumImages
          selectedImages={selectedImages}
          removeImage={(img) => onImageSelect(img, true)} // true for removing
        />
        <AlbumLabels selectedImages={selectedImages} />
      </div>
    </aside>
  );
};

export default AlbumGenerator;
