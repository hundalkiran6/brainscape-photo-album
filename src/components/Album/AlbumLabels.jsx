import React from "react";
import AlbumLabel from "./AlbumLabel";

import "./Album.css";

const AlbumLabels = ({ selectedImages }) => {
  return (
    <div className="album-labels">
      {selectedImages.map((img, index) => (
        <AlbumLabel key={img.id} img={img} index={index} />
      ))}
    </div>
  );
};
export default AlbumLabels;
