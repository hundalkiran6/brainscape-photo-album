const AlbumImageWrapper = ({ img, removeImage }) => {
  return (
    <section className="album-image-wrapper">
      <img src={img.url} alt={img.title} className="album-image" />
      <button onClick={() => removeImage(img)} aria-label="Remove image">
        x
      </button>
    </section>
  );
};
export default AlbumImageWrapper;
