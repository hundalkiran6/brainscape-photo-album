import React from "react";
import AlbumImageWrapper from "./AlbumImageWrapper";

import "./Album.css";

const AlbumImages = ({ selectedImages, removeImage }) => {
  return (
    <section className="album-images">
      {selectedImages.map((img) => (
        <AlbumImageWrapper
          key={img.id}
          img={img}
          removeImage={() => removeImage(img, true)}
        />
      ))}
    </section>
  );
};
export default AlbumImages;
