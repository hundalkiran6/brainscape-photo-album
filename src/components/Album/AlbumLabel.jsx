import React from "react";

import "./Album.css";

const AlbumLabel = ({ img, index }) => {
  return (
    <div className="album-label">
      {index + 1}. {img.title}
    </div>
  );
};
export default AlbumLabel;
