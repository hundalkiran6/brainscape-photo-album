import React from "react";
import { useDrag } from "react-dnd";
import { Helmet } from "react-helmet-async";

import "./ImageItem.css";

const ImageItem = ({ image }) => {
  const [{ isDragging }, drag] = useDrag(() => ({
    type: "IMAGE",
    item: { image },
    collect: (monitor) => ({
      isDragging: !!monitor.isDragging(),
    }),
  }));

  return (
    <section ref={drag} style={{ opacity: isDragging ? 0.5 : 1 }}>
      <Helmet>
        <script type="application/ld+json">
          {JSON.stringify({
            "@context": "http://schema.org",
            "@type": "ImageObject",
            user: image.user,
            contentUrl: image.url,
            description: image.description,
            name: image.title,
          })}
        </script>
      </Helmet>
      <img
        src={image.url}
        alt={image.title}
        width="200"
        height="200"
        loading="lazy"
      />
    </section>
  );
};

export default ImageItem;
