import React from "react";
import ImageItem from "../ImageItem/ImageItem";

import "./ImageGallery.css";

const ImageGallery = ({ images, onImageSelect }) => {
  return (
    <main className="image-gallery">
      {images.map((img) => (
        <ImageItem key={img.id} image={img} onImageSelect={onImageSelect} />
      ))}
    </main>
  );
};

export default ImageGallery;
