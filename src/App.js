import React, { useState, useEffect, useCallback, Suspense } from "react";
import { Helmet, HelmetProvider } from "react-helmet-async";
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";

import { fetchImages } from "./services/apiService";
import Header from "./components/Header/Header";
import {
  ImageGallery,
  AlbumGenerator,
} from "./components/LazyLoadedComponents";

import "./App.css";

const useFetchImages = () => {
  const [images, setImages] = useState([]);

  useEffect(() => {
    const fetchAndSetImages = async () => {
      try {
        const fetchedImages = await fetchImages();
        const imagesWithIndex = fetchedImages.map((img, index) => ({
          ...img,
          originalIndex: index,
        }));
        setImages(imagesWithIndex);
      } catch (error) {
        console.error("Error fetching images:", error);
      }
    };

    fetchAndSetImages();
  }, []);

  return [images, setImages];
};

function App() {
  const [images, setImages] = useFetchImages();
  const [selectedImages, setSelectedImages] = useState([]);

  // using useCallback to prevent unnecessary re-renders
  const handleImageSelection = useCallback(
    (image, isSelected) => {
      setSelectedImages((prevSelected) =>
        isSelected
          ? prevSelected.filter((img) => img.id !== image.id)
          : [...prevSelected, image]
      );
      setImages((prevImages) =>
        isSelected
          ? [...prevImages, image]
          : prevImages.filter((img) => img.id !== image.id)
      );
    },
    [setImages]
  );

  return (
    <HelmetProvider>
      <DndProvider backend={HTML5Backend}>
        <div className="App">
          <Helmet>
            <title>Brainscape Photo App</title>
            <meta
              name="description"
              content="Front End Engineer Candidate Coding Exercise"
            />
            <link rel="canonical" href={process.env.REACT_APP_SITE_URL} />
            <meta name="robots" content="index,follow" />
          </Helmet>
          <Header />
          <div className="content-container">
            <Suspense fallback={<div>Loading...</div>}>
              <ImageGallery
                images={images}
                onImageSelect={handleImageSelection}
              />
              <AlbumGenerator
                selectedImages={selectedImages}
                onImageSelect={handleImageSelection}
              />
            </Suspense>
          </div>
        </div>
      </DndProvider>
    </HelmetProvider>
  );
}

export default App;
