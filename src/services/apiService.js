export const fetchImages = async () => {
  const API_URL =
    "https://api.slingacademy.com/v1/sample-data/photos?offset=0&limit=100";

  // Use sessionStorage for persistent caching during the session
  const cachedData = sessionStorage.getItem(API_URL);
  if (cachedData) {
    return JSON.parse(cachedData);
  }

  try {
    const response = await fetch(API_URL);
    if (!response.ok) throw new Error(`HTTP error: ${response.status}`);
    const data = await response.json();
    sessionStorage.setItem(API_URL, JSON.stringify(data.photos));
    return data.photos;
  } catch (error) {
    console.error("Failed to fetch images:", error);
    throw error;
  }
};
